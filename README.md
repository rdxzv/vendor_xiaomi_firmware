# vendor_xiaomi_firmware

Stock firmware images from Xiaomi POCO X3 NFC (surya), Global images only.

### Current version
`14.0.2.0.SJGMIXM (Android 12)`
https://bigota.d.miui.com/V14.0.2.0.SJGMIXM/miui_SURYAGlobal_V14.0.2.0.SJGMIXM_9aaad01ef3_12.0.zip

### How to use?

1. Clone this repo to `vendor/xiaomi/firmware`.

2. Inherit the config makefile from `device.mk`.

```
# Firmware
$(call inherit-product, vendor/xiaomi/firmware/config.mk)
```
