# Copyright (C) 2023 Paranoid Android
# SPDX-License-Identifier: Apache-2.0

FIRMWARE_PATH := vendor/xiaomi/firmware

FIRMWARE_IMAGES := \
    $(notdir $(wildcard $(FIRMWARE_PATH)/images/*))

PRODUCT_SOONG_NAMESPACES += \
    $(FIRMWARE_PATH)
